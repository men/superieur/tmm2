package fr.gouv.mesri.tmm2.repository;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import fr.gouv.mesri.tmm2.model.Candidature;
import fr.gouv.mesri.tmm2.model.algo.FormationCandidatableProjection;

@Repository
public interface AlgoRepository extends JpaRepository<Candidature, Long> {

    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature SET previous_statut = statut, statut = 'REFUSEE', modified_by = 'system', modified_date = NOW(), raison = 'EXPIRATION_DELAI_1' WHERE (statut = 'EN_ATTENTE_DE_REPONSE' OR statut = 'EN_ATTENTE_DE_REPONSE_CONSERVE') AND NOW() > expected_response_date", nativeQuery = true)
    int updateStatutRefuseeExpectedResponseDateExpired();

    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature c SET previous_statut = c.statut, statut = 'NON_CLASSEE', raison=raison_nc, modified_by = 'system', modified_date = NOW() FROM formation_candidatable fc WHERE c.ifc = fc.ifc AND c.statut = 'CONFIRMEE' AND c.classement = -1 AND fc.statut = 'VALIDEE'", nativeQuery = true)
    int updateStatutNonClassee();

    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature c SET previous_statut = c.statut, statut = 'NON_CLASSEE', raison=raison_nc, modified_by = 'system', modified_date = NOW() FROM formation_candidatable fc LEFT JOIN pccd ON fc.pccd_id = pccd.id WHERE c.ifc = fc.ifc AND c.statut = 'CONFIRMEE' AND c.classement = -1 AND fc.statut = 'VALIDEE' AND fc.alternance IS TRUE AND pccd.mixte IS NOT TRUE", nativeQuery = true)
    int updateStatutNonClasseeAlternance();


    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature c SET confirmee = TRUE WHERE c.statut = 'CONFIRMEE' AND confirmee IS NOT TRUE", nativeQuery = true)
    int updateConfirmeeEndOfCandidatureTamponPhase();


    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature c SET previous_statut = c.statut, statut = 'REFUSEE', modified_by = 'system', modified_date = NOW(), raison = 'EXPIRATION_PUBLICATION_CLASSEMENT' FROM formation_candidatable fc WHERE c.ifc = fc.ifc AND c.statut = 'CONFIRMEE'", nativeQuery = true)
    int updateRefuseeFormationWOClassementEnOfAdmissionPhase();


    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature c SET archivee = TRUE WHERE c.statut = 'LISTE_ATTENTE' AND archivee IS NOT TRUE", nativeQuery = true)
    int updateLAArchiveeEndOfAdmissionPhase();

    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature c SET previous_statut = c.statut, statut = 'REFUSEE', hierarchie = null, modified_by = 'system', modified_date = NOW(), raison = 'EXPIRATION_HIERARCHISATION' FROM formation_candidatable fc WHERE fc.ifc = c.ifc AND fc.alternance = false AND c.statut = 'LISTE_ATTENTE' AND c.dossier_id IN (SELECT DISTINCT c1.dossier_id FROM candidature c1 JOIN formation_candidatable fc1 ON fc1.ifc = c1.ifc WHERE c1.statut = 'LISTE_ATTENTE' AND fc1.alternance = false AND c1.hierarchie IS NULL)", nativeQuery = true)
    int updateStatutRefuseeEndOfHierarchisationPhase();

    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature c SET previous_statut = c.statut, statut = 'REFUSEE', modified_by = 'system', modified_date = NOW(), raison = 'EXPIRATION_GDD' FROM formation_candidatable fc WHERE fc.ifc = c.ifc AND fc.alternance = false AND c.statut = 'LISTE_ATTENTE'", nativeQuery = true)
    int updateStatutRefuseeEndOfGddPhase();

    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature c SET previous_statut = c.statut, statut = 'REFUSEE', modified_by = 'system', modified_date = NOW(), raison = 'EXPIRATION_ADMISSION_ALTERNANCE' FROM formation_candidatable fc WHERE fc.ifc = c.ifc AND fc.alternance = true AND c.statut = 'LISTE_ATTENTE'", nativeQuery = true)
    int updateStatutRefuseeEndOfAdmissionAlternancePhase();



    @Query(value = "SELECT fc.ifc, fc.uai, fc.col, fc.donnee_appel as donneeAppel, COUNT(c) AS nbProposals, fc.alternance FROM formation_candidatable fc LEFT JOIN candidature c ON fc.ifc = c.ifc AND (c.statut IN ('EN_ATTENTE_DE_REPONSE', 'ACCEPTEE_NON_DEFINITIVEMENT', 'ACCEPTEE_DEFINITIVEMENT') OR (c.statut = 'EN_ATTENTE_DE_REPONSE_CONSERVE' AND fc.alternance IS TRUE)) WHERE fc.statut = 'VALIDEE' AND (fc.algo_admission_dernier_passage_date < :time OR fc.algo_admission_dernier_passage_date IS NULL) AND fc.classement_file IS NOT NULL GROUP BY fc.ifc", nativeQuery = true)
    List<FormationCandidatableProjection> findAllForAdmissionAlgo(@Param("time") LocalDateTime time);

    @Query(value = "SELECT fc.ifc, fc.uai, fc.col, fc.donnee_appel as donneeAppel, COUNT(c) AS nbProposals, fc.alternance FROM formation_candidatable fc LEFT JOIN candidature c ON fc.ifc = c.ifc AND c.statut IN ('EN_ATTENTE_DE_REPONSE', 'ACCEPTEE_NON_DEFINITIVEMENT', 'EN_ATTENTE_DE_REPONSE_CONSERVE', 'ACCEPTEE_DEFINITIVEMENT') LEFT JOIN pccd ON fc.pccd_id = pccd.id WHERE fc.statut = 'VALIDEE' AND (fc.algo_admission_dernier_passage_date < :time OR fc.algo_admission_dernier_passage_date IS NULL) AND fc.classement_file IS NOT NULL AND fc.alternance = true AND pccd.mixte IS NOT TRUE GROUP BY fc.ifc", nativeQuery = true)
    List<FormationCandidatableProjection> findAllForAdmissionAlgoAlternance(@Param("time") LocalDateTime time);


    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature SET proposition = TRUE WHERE statut ='EN_ATTENTE_DE_REPONSE' AND proposition IS FALSE", nativeQuery = true)
    int updateCandidaturesPropositionFlag();


    @Transactional(value = TxType.REQUIRED)
    @Modifying
    @Query(value = "UPDATE candidature c SET previous_statut = c.statut, statut = 'REFUSEE', modified_by = 'system', modified_date = NOW(), raison = 'PROPOSITION_RANG_INFERIEUR' FROM formation_candidatable fc, candidature c2, formation_candidatable fc2 WHERE c.dossier_id = c2.dossier_id AND c.id <> c2.id AND c2.proposition = TRUE AND c.statut IN ('LISTE_ATTENTE', 'EN_ATTENTE_DE_REPONSE_CONSERVE', 'EN_ATTENTE_DE_REPONSE') AND c.hierarchie > c2.hierarchie AND fc.ifc = c.ifc AND fc2.ifc = c2.ifc AND fc.alternance = FALSE AND fc2.alternance = FALSE", nativeQuery = true)
    int updateStatutRefuseeLowerHierarchisation();

    @Modifying
    @Query(value = "TRUNCATE tmp_candidature", nativeQuery = true)
    void truncateCandidatureTmp();


    @Query("SELECT NEW fr.gouv.mesri.tmm2.model.Candidature(c.id, c.ifc, c.statut, c.classement, c.listeAttentePosition, c.hierarchie) FROM Candidature c JOIN FormationCandidatable fc ON c.ifc = fc.ifc WHERE fc.uai = :uai AND (c.statut IN ('CONFIRMEE', 'LISTE_ATTENTE') OR (c.statut = 'EN_ATTENTE_DE_REPONSE_CONSERVE' AND fc.alternance IS FALSE))")
    List<Candidature> findAllForAdmissionAlgoByUai(String uai);



    @Modifying
    @Query(value = "UPDATE candidature SET previous_statut = candidature.statut, modified_by = 'system', modified_date = NOW(), liste_attente_position = tmp.liste_attente_position, statut = tmp.statut, expected_response_date = tmp.expected_response_date FROM tmp_candidature tmp WHERE candidature.id = tmp.id AND tmp.statut IS NOT NULL", nativeQuery = true)
    void updateCandidatureFromTmp();

    @Modifying
    @Query(value = "UPDATE candidature SET modified_by = 'system', modified_date = NOW(), liste_attente_position = tmp.liste_attente_position FROM tmp_candidature tmp WHERE candidature.id = tmp.id AND tmp.statut IS NULL", nativeQuery = true)
    void updateCandidaturePositionFromTmp();

    @Modifying
    @Query(value = "UPDATE formation_candidatable fc SET algo_admission_dernier_passage_date = :time WHERE ifc IN :ifcs", nativeQuery = true)
    void updateAlgoAdmissionDernierPassageDate(@Param("ifcs") List<String> ifcs, @Param("time") LocalDateTime time);




    @Modifying
    @Query(value = "UPDATE pccd SET nb_candidats = cmpt.nb_candidats FROM (SELECT pccd_id, COUNT(DISTINCT c.dossier_id) AS nb_candidats FROM formation_candidatable fc LEFT JOIN candidature c on c.ifc = fc.ifc AND (c.statut = 'CONFIRMEE' OR c.confirmee) WHERE fc.uai = COALESCE(CAST(:uai AS TEXT),fc.uai) GROUP BY pccd_id) cmpt WHERE pccd.id = cmpt.pccd_id", nativeQuery = true)
    void updatePccdCounters(@Nullable @Param("uai") String uai);



    @Modifying
    @Query(value = """
            UPDATE formation_candidatable
            SET
                nb_candidatures_total = cmpt.nb_candidatures_total,
                nb_candidatures_confirmees = cmpt.nb_candidatures_confirmees,
                nb_candidatures_classees = cmpt.nb_candidatures_classees,
                nb_candidatures_ear = cmpt.nb_candidatures_ear,
                nb_candidatures_la = cmpt.nb_candidatures_la,
                nb_candidatures_acc_non_def = cmpt.nb_candidatures_acc_non_def,
                nb_candidatures_acc_def = cmpt.nb_candidatures_acc_def,
                nb_propositions_refusees = cmpt.nb_propositions_refusees,
                rang_dernier_appele = cmpt.rang_dernier_appele
            FROM (
            SELECT fc.ifc,
                COUNT(c) AS nb_candidatures_total,
                COUNT(c) FILTER (WHERE c.statut = 'CONFIRMEE' OR c.confirmee) AS nb_candidatures_confirmees,
                COUNT(c) FILTER (WHERE c.classement is not null AND c.classement != -1) AS nb_candidatures_classees,
                COUNT(c) FILTER (WHERE c.statut = 'EN_ATTENTE_DE_REPONSE') AS nb_candidatures_ear,
                COUNT(c) FILTER (WHERE c.statut = 'LISTE_ATTENTE' OR (c.statut = 'EN_ATTENTE_DE_REPONSE_CONSERVE' AND fc.alternance = FALSE)) AS nb_candidatures_la,
                COUNT(c) FILTER (WHERE c.statut = 'ACCEPTEE_NON_DEFINITIVEMENT' OR (c.statut IN ('EN_ATTENTE_DE_REPONSE', 'EN_ATTENTE_DE_REPONSE_CONSERVE') AND fc.alternance = TRUE)) AS nb_candidatures_acc_non_def,
                COUNT(c) FILTER (WHERE c.statut = 'ACCEPTEE_DEFINITIVEMENT') AS nb_candidatures_acc_def,
                COUNT(c) FILTER (WHERE c.statut = 'REFUSEE' AND c.proposition) AS nb_propositions_refusees,
                GREATEST(fc.rang_dernier_appele, MAX(c.classement) FILTER (WHERE c.statut = 'EN_ATTENTE_DE_REPONSE')) AS rang_dernier_appele
            FROM formation_candidatable fc LEFT JOIN candidature c on c.ifc = fc.ifc WHERE fc.uai = COALESCE(CAST(:uai AS TEXT), fc.uai) GROUP BY fc.ifc) AS cmpt
            WHERE cmpt.ifc = formation_candidatable.ifc
            """, nativeQuery = true)
    void updateFcCounters(@Nullable @Param("uai") String uai);

}

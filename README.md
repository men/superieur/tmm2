Ce dépôt concerne le processus de dépilement des propositions d’admission effectué via la plateforme nationale de candidature en première année de master. Il s’agit de publier la partie du code-source de la plateforme Mon Master qui gère cette procédure. 

### Description des fichiers déposés

Le dépôt contient le code Java permettant

 - le calcul de l'ordre d'appel
 - le calcul des propositions de formations
 - l'application des démissions automatiques des vœux archivés pendant la phase de gestion des démissions (GDD).


Le dépôt comprend les fichiers :
- `AlgoServiceImpl.java`
- `AlgoRepository.java`
- Présentation des algorithmes

Contact: contact.monmaster@enseignementsup.gouv.fr

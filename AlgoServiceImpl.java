package fr.gouv.mesri.tmm2.service;

import static fr.gouv.mesri.tmm2.model.Candidature.CandidatureStatut.ACCEPTEE_DEFINITIVEMENT;
import static fr.gouv.mesri.tmm2.model.Candidature.CandidatureStatut.ACCEPTEE_NON_DEFINITIVEMENT;
import static fr.gouv.mesri.tmm2.model.Candidature.CandidatureStatut.CONFIRMEE;
import static fr.gouv.mesri.tmm2.model.Candidature.CandidatureStatut.EN_ATTENTE_DE_REPONSE;
import static fr.gouv.mesri.tmm2.model.Candidature.CandidatureStatut.LISTE_ATTENTE;
import static fr.gouv.mesri.tmm2.security.Phase.ADMISSION_ALTERNANCE;
import static fr.gouv.mesri.tmm2.security.Phase.ADMISSION_PRINCIPALE;
import static fr.gouv.mesri.tmm2.security.Phase.CONSULTATION;
import static fr.gouv.mesri.tmm2.security.Phase.CONSULTATION_ALTERNANCE;
import static fr.gouv.mesri.tmm2.security.Phase.EXAMEN;
import static fr.gouv.mesri.tmm2.security.Phase.GDD;
import static fr.gouv.mesri.tmm2.security.Phase.HIERARCHISATION;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.mesri.tmm2.exception.ResourceNotFoundException;
import fr.gouv.mesri.tmm2.exception.ResourcePreconditionFailException;
import fr.gouv.mesri.tmm2.model.Candidature;
import fr.gouv.mesri.tmm2.model.Candidature.RefuseeRaison;
import fr.gouv.mesri.tmm2.model.algo.FormationCandidatableProjection;
import fr.gouv.mesri.tmm2.model.algo.TmpCandidature;
import fr.gouv.mesri.tmm2.repository.AlgoRepository;
import fr.gouv.mesri.tmm2.repository.CandidatureRepository;
import fr.gouv.mesri.tmm2.security.Phase;
import lombok.extern.log4j.Log4j2;


@Log4j2
@Service
public class AlgoServiceImpl implements AlgoService {

    @Value("${candidature.algo.finPhaseAdmission.threadPool}")
    Integer endOfAdmissionPhaseThreadPool;

    private @Autowired CandidatureRepository candidatureRepository;

    private @Lazy @Autowired AlgoService algoService;
    private @Autowired PhaseService phaseService;
    private @Autowired @Lazy EmailCandidatureService emailCandidatureService;
    private @Autowired StatistiqueService statistiqueService;
    private @Autowired AppConfigService appConfigService;
    private @Autowired AlgoRepository algoRepository;

    private @Autowired JdbcTemplate jdbcTemplate;

    private LocalDate expectedResponseDateClassic;
    private LocalDate expectedResponseDateAlternance;


    @Override
    public void algoAdmission(LocalDateTime time) throws ResourceNotFoundException, ResourcePreconditionFailException, InterruptedException {
        expectedResponseDateClassic = getNextExpectedResponseDateClassic();
        expectedResponseDateAlternance = getNextExpectedResponseDateAlternance();

        int nbCandidaturesEchues = algoRepository.updateStatutRefuseeExpectedResponseDateExpired();
        log.info("PROCESS algoAdmission - Nombre de candidatures échues refusées : {}", nbCandidaturesEchues);
        if (phaseService.hasAnyValidPhase(ADMISSION_PRINCIPALE)) {
            int nbCandidaturesNonClassees = algoRepository.updateStatutNonClassee();
            log.info("PROCESS algoAdmission - Nombre de candidatures non classées : {}", nbCandidaturesNonClassees);
        } else if (phaseService.hasAnyValidPhase(ADMISSION_ALTERNANCE)) {
            int nbCandidaturesNonClassees = algoRepository.updateStatutNonClasseeAlternance();
            log.info("PROCESS algoAdmission - Nombre de candidatures alternance non classées : {}", nbCandidaturesNonClassees);
        }


        if (phaseService.hasAnyValidPhase(EXAMEN)) {
            log.info("START PROCESS runFinPhaseCandidatureTampon");
            endOfCandidatureTamponPhase();
            log.info("END PROCESS runFinPhaseCandidatureTampon");
        } else if (phaseService.hasAnyValidPhase(HIERARCHISATION)) {
            log.info("START PROCESS runFinPhaseAdmission");
            endOfAdmissionPhase();
            log.info("END PROCESS runFinPhaseAdmission");
        } else if (phaseService.hasAnyValidPhase(GDD)) {
            log.info("START PROCESS runFinPhaseHierarchisation");
            endOfHierarchisationPhase();
            log.info("END PROCESS runFinPhaseHierarchisation");
        } else if (phaseService.hasAnyValidPhase(CONSULTATION)) {
            log.info("START PROCESS runFinPhaseGdd");
            endOfGddPhase();
            log.info("END PROCESS runFinPhaseGdd");
        }
        if (phaseService.hasAnyValidPhase(CONSULTATION_ALTERNANCE)) {
            log.info("START PROCESS runFinPhaseAdmissionAlternance");
            endOfAdmissionAlternancePhase();
            log.info("END PROCESS runFinPhaseAdmissionAlternance");
        }



        int i = 0;
        do {
            List<FormationCandidatableProjection> fcs = new ArrayList<>();
            if (phaseService.hasAnyValidPhase(ADMISSION_PRINCIPALE, GDD) && expectedResponseDateClassic != null && !expectedResponseDateClassic.isAfter(phaseService.endDateOf(phaseService.hasValidPhase(ADMISSION_PRINCIPALE) ? ADMISSION_PRINCIPALE : GDD))) {
                fcs.addAll(algoRepository.findAllForAdmissionAlgo(time.plusDays(i)));
            } else if (!expectedResponseDateAlternance.isAfter(phaseService.endDateOf(ADMISSION_ALTERNANCE))) {
                fcs.addAll(algoRepository.findAllForAdmissionAlgoAlternance(time.plusDays(i)));
            }
            if (++i != 1)
                log.info("PROCESS algoAdmission - Algo pass {}", i);
                fcs.stream().map(FormationCandidatableProjection::getUai).distinct().forEach(uai -> {
                try {
                    List<FormationCandidatableProjection> uaiFcs = fcs.stream().filter(fc -> fc.getUai().equals(uai)).collect(Collectors.toList());
                    algoService.algoAdmissionForUai(time, uai, uaiFcs);
                } catch (Exception e) {
                    log.error(e);
                    e.printStackTrace();
                }
            });
            int nbNewPropositions = algoRepository.updateCandidaturesPropositionFlag();
            log.info("PROCESS algoAdmission - Nombre de propositions crées : {}", nbNewPropositions);
        } while (phaseService.hasAnyValidPhase(GDD) && algoRepository.updateStatutRefuseeLowerHierarchisation() != 0);

        log.info("END PROCESS algoAdmission");
    }

    @Override
    @Transactional
    public void algoAdmissionForUai(LocalDateTime time, String uai, List<FormationCandidatableProjection> fcs) {
        algoRepository.truncateCandidatureTmp();
        List<TmpCandidature> changes = new ArrayList<>();
        List<Candidature> uaiCandidatures = algoRepository.findAllForAdmissionAlgoByUai(uai);
        Map<String, List<Candidature>> byIfcCandidatures = new HashMap<>();
        uaiCandidatures.stream().forEach(c -> {
            if (!byIfcCandidatures.containsKey(c.getIfc())) {
                byIfcCandidatures.put(c.getIfc(), new ArrayList<>());
            }
            byIfcCandidatures.get(c.getIfc()).add(c);
        });
        for (FormationCandidatableProjection fc : fcs) {
            if (byIfcCandidatures.get(fc.getIfc()) == null)
                continue;
            changes.addAll(algoAdmissionForIfc(fc, byIfcCandidatures.get(fc.getIfc())));
        }
        if (!changes.isEmpty()) {
            insertTmpCandidatures(changes);
            algoRepository.updateCandidatureFromTmp();
            algoRepository.updateCandidaturePositionFromTmp();
        }

        algoRepository.updateAlgoAdmissionDernierPassageDate(fcs.stream().map(FormationCandidatableProjection::getIfc).toList(), time);

        log.info("PROCESS algoAdmission - End Computing Uai {} with {} changes in candidatures", uai, changes.size());
    }

    private List<TmpCandidature> algoAdmissionForIfc(FormationCandidatableProjection fc, List<Candidature> candidatures) {
        log.debug("PROCESS algoAdmission - Start Computing IFC {}", fc.getIfc());
        return recalcListeAttentePosition(fc, candidatures);
    }


    private List<TmpCandidature> recalcListeAttentePosition(FormationCandidatableProjection fc, List<Candidature> candidatures) {
        LocalDate expectedResponseDate = fc.isAlternance() ? expectedResponseDateAlternance : expectedResponseDateClassic;
        List<TmpCandidature> changes = new ArrayList<>();
        AtomicInteger capacity = new AtomicInteger(1 - getDACOL(fc) + fc.getNbProposals());
        candidatures.stream()
                    .filter(c -> c.getClassement() != null && c.getClassement() > 0)
                    .sorted(Comparator.comparingInt(Candidature::getClassement))
                    .forEach(c -> {
            Integer position = capacity.getAndIncrement();
            position = position < 0 ? 0 : position;
            if (position <= 0) {
                if (!(phaseService.hasValidPhase(GDD) && !fc.isAlternance() && c.getHierarchie() == null)) {
                    changes.add(new TmpCandidature(c.getId(), position, EN_ATTENTE_DE_REPONSE, expectedResponseDate));
                } else {
                    log.error("PROCESS algoAdmission - Proposition classique non créée car non hiérarchisée. Candidature ID {} de l'IFC {}", c.getId(), c.getIfc());
                }
            } else if (c.getListeAttentePosition() == null || position < c.getListeAttentePosition()) {
                changes.add(new TmpCandidature(c.getId(), position, CONFIRMEE == c.getStatut() ? LISTE_ATTENTE : null, null));
            }
        });
        return changes;
    }
    private int getDACOL(FormationCandidatableProjection fc) {
        return ((!phaseService.hasValidPhase(GDD) || fc.isAlternance()) && fc.getDonneeAppel() != null) ? fc.getDonneeAppel() : fc.getCol();
    }


    private void insertTmpCandidatures(List<TmpCandidature> changes) {
        jdbcTemplate.batchUpdate("INSERT INTO tmp_candidature (id, liste_attente_position, statut, expected_response_date) VALUES (?, ?, ?, ?)", changes, 500, (PreparedStatement ps, TmpCandidature c) -> {
            ps.setLong(1, c.getId());
            ps.setInt(2, c.getListeAttentePosition());
            ps.setString(3, c.getStatut() == null ? null : c.getStatut());
            ps.setDate(4, (c.getExpectedResponseDate() == null ? null : Date.valueOf(c.getExpectedResponseDate().toString())));
        });
    }




    private void endOfAdmissionPhase() throws InterruptedException {
        int nbCandidatureWOClassement = algoRepository.updateRefuseeFormationWOClassementEnOfAdmissionPhase();
        log.info("PROCESS runFinPhaseAdmission - Nombre de candidatures refusées car sans classement : {}", nbCandidatureWOClassement);

        List<Candidature> toAcceptCandidatures = candidatureRepository.findByStatutAndFormationCandidatableAlternance(ACCEPTEE_NON_DEFINITIVEMENT, false);
        log.info("PROCESS runFinPhaseAdmission - Nombre de candidatures acceptées définitivement car acceptées non définitivement : {}", toAcceptCandidatures.size());
        ExecutorService executor = Executors.newFixedThreadPool(endOfAdmissionPhaseThreadPool);
        toAcceptCandidatures.stream().forEach(c -> {
            Runnable worker = () -> {
                try {
                    algoService.endOfAdmissionPhaseAcceptCandidatureACCNDEF(c);
                } catch (Exception e) {
                    log.error("PROCESS runFinPhaseAdmission - Candidature ID {} de l'IFC {} n'a pas pu être acceptée définitivement : {}", c.getId(), c.getIfc(), e.getMessage());
                }
            };
            executor.execute(worker);
        });
        // This will make the executor accept no new threads
        // and finish all existing threads in the queue
        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.DAYS);

        int nbCandidatureLAArchivees = algoRepository.updateLAArchiveeEndOfAdmissionPhase();
        log.info("PROCESS runFinPhaseAdmission - Nombre de candidatures en liste d'attente archivées : {}", nbCandidatureLAArchivees);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void endOfAdmissionPhaseAcceptCandidatureACCNDEF(Candidature toAcceptCandidature) {
        // Need to get candidature with the new transaction (New transaction session)
        Candidature candidature = candidatureRepository.findById(toAcceptCandidature.getId()).get();
        if (candidatureRepository.existsByDossierIdAndStatut(candidature.getDossierId(), ACCEPTEE_DEFINITIVEMENT))
            throw new ResourcePreconditionFailException("Une candidature avec un statut ACCEPTEE_DEFINITIVEMENT existe déjà pour le candidat " + candidature.getDossierId());
        candidatureRepository.updateRefuseOtherCandidaturesFollowingAccepteeDefinitivementClassic(candidature.getId(), candidature.getDossierId(), RefuseeRaison.ACCEPTATION_DEFINITIVE_AUTRE_PROPOSITION_FIN_PHASE_ADMISSION);
        emailCandidatureService.sendMailForCandidatureAcceptationDefinitive(candidature);
        candidature.setPreviousStatut(candidature.getStatut());
        candidature.setStatut(ACCEPTEE_DEFINITIVEMENT);
    }




    private void endOfCandidatureTamponPhase() {
        int nbCandidatureConfirmees = algoRepository.updateConfirmeeEndOfCandidatureTamponPhase();
        log.info("PROCESS runFinCandidatureTamponPhase - Nombre de candidatures confirmées  : {}", nbCandidatureConfirmees);
    }

    private void endOfHierarchisationPhase() {
        int nb = algoRepository.updateStatutRefuseeEndOfHierarchisationPhase();
        log.info("PROCESS runFinPhaseHierarchisation - Nombre de candidatures refusées car non entierement hiérarchisées : {}", nb);
    }

    private void endOfGddPhase() {
        int nb = algoRepository.updateStatutRefuseeEndOfGddPhase();
        log.info("PROCESS runFinPhaseGdd - Nombre de candidatures refusées car encore en liste d'attente : {}", nb);
    }

    private void endOfAdmissionAlternancePhase() {
        int nb = algoRepository.updateStatutRefuseeEndOfAdmissionAlternancePhase();
        log.info("PROCESS runFinPhaseAdmissionAlternance - Nombre de candidatures refusées car encore en liste d'attente : {}", nb);
    }



    @Transactional
    public void updateCounters() {
        log.info("START PROCESS updateCounters");
        algoRepository.updatePccdCounters(null);
        algoRepository.updateFcCounters(null);
        log.info("END PROCESS updateCounters");
    };

    @Transactional
    public void updateCountersByUai(String uai) {
        algoRepository.updatePccdCounters(uai);
        algoRepository.updateFcCounters(uai);
    };

    @Transactional
    public void updateStats() {
        updateCounters();

        log.info("START PROCESS updateStats");
        StatistiqueService.VIEW_NAME_LIST.forEach(view -> statistiqueService.generateStats(view));
        log.info("END PROCESS updateStats");
    }


    private boolean isTodayBetween(LocalDate start, LocalDate end) {
        return (LocalDate.now().equals(start) || LocalDate.now().isAfter(start)) && (LocalDate.now().equals(end) || LocalDate.now().isBefore(end));
    }

    @Override
    public boolean isToBeExecuted() {
        return isTodayBetween(phaseService.startDateOf(Phase.EXAMEN), phaseService.endDateOf(CONSULTATION));
    }


    @Override
    public LocalDate getNextExpectedResponseDateClassic() {
        LocalDate expectedResponseDate;
        if (phaseService.hasAnyValidPhase(ADMISSION_PRINCIPALE)) {
            LocalDate admissionPrincipaleStartResponse = phaseService.startDateOf(ADMISSION_PRINCIPALE).plusDays(appConfigService.getInt("candidature.algoAdmission.phaseAdmission.delaiInitialAvantReponse.classique"));
            expectedResponseDate = ObjectUtils.max(admissionPrincipaleStartResponse, LocalDate.now().plusDays(appConfigService.getInt("candidature.algoAdmission.phaseAdmission.delai1AvantReponse.classique")));
        } else if (phaseService.hasAnyValidPhase(GDD)) {
            LocalDate gddStartResponse = appConfigService.getDate("candidature.algoAdmission.phaseGDD.premierDelaiAvantReponse.classique");
            expectedResponseDate = ObjectUtils.max(gddStartResponse, LocalDate.now().plusDays(appConfigService.getInt("candidature.algoAdmission.phaseGDD.deuxiemeDelaiAvantReponse.classique")));
        } else {
            return null;
        }
        return expectedResponseDate;
    }

    @Override
    public LocalDate getNextExpectedResponseDateAlternance() {
        LocalDate expectedResponseDate;
        if (phaseService.hasAnyValidPhase(EXAMEN, ADMISSION_PRINCIPALE)) {
            LocalDate admissionPrincipaleStartResponse = phaseService.startDateOf(ADMISSION_PRINCIPALE).plusDays(appConfigService.getInt("candidature.algoAdmission.phaseAdmission.delaiInitialAvantReponse.alternance"));
            expectedResponseDate = ObjectUtils.max(admissionPrincipaleStartResponse, LocalDate.now().plusDays(appConfigService.getInt("candidature.algoAdmission.phaseAdmission.delai1AvantReponse.alternance")));
        } else {
            LocalDate delai2Date = appConfigService.getDate("candidature.algoAdmission.phaseAdmission.delai2AvantReponseDate.alternance");
            expectedResponseDate = LocalDate.now().plusDays(appConfigService.getInt("candidature.algoAdmission.phaseAdmission." + (LocalDate.now().isBefore(delai2Date) ? "delai1AvantReponse" : "delai2AvantReponse") + ".alternance"));
        }
        return expectedResponseDate;
    }
}
